/*** Import ***/
import Vue from 'vue';
import App from './App.vue';
import VueRouter from 'vue-router';
// Import Routes
import routes from './routes'
// Import Bootstrap and Bootstrap Icons
import { BootstrapVue, BootstrapVueIcons } from 'bootstrap-vue'
// Import Bootstrap an BootstrapVue CSS files (order is important)
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'
// Import Calendar
import vueCalendar from "vue2-simple-calendar";
import "../src/assets/css/vue2-simple-calendar.css";
// Import Sileo
import sileo from 'sileo'
// Import VueEditor
import VueEditor from "vue2-editor";

// Configure Sileo base URL (the URL of the server with Sileo)
sileo.defaults.baseUrl = 'http://localhost:8000'

const token = localStorage.getItem('auth_token')
if (token) {
  sileo.defaults.headers['Authorization'] = token
}

// Routes
const router = new VueRouter({
  mode: 'history',
  routes: routes,
});

router.beforeEach((to, from, next) => {
  let auth_token = localStorage.getItem('auth_token')
  if (to.name == 'register' || to.name == 'login'){
    if (auth_token) {
      next({ name: 'home' })
    } else {
      next()
    }
  }
  else if (to.name !== 'login' && !auth_token) {
    next({ name: 'login' })
  }
  else {
    next()
  }
})


/*** Use ***/
Vue.use(VueRouter)
Vue.use(BootstrapVue)
Vue.use(BootstrapVueIcons)
Vue.use(vueCalendar, {
  locale: "fil",
  languages: {
    fil: {
      showMore: "...",
      dayNameShort: ["Sun", "Mon", "Tue", "Wed", "Thur", "Fri", "Sat"],
      dayNameLong: ["Sun", "Mon", "Tue", "Wed", "Thur", "Fri", "Sat"],
      monthNameShort: [ "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December" ],
      monthNameLong: [ "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December" ]
    }
  }
});
Vue.use(VueEditor)

Vue.config.productionTip = false;


/*** Vue ***/
new Vue({
  router,
  render: (h) => h(App),
}).$mount('#app');
