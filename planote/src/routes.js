import Home from './pages/Home.vue';
import Board from './pages/Board.vue';
import Plan from './pages/Plan.vue';
import Login from './pages/Login.vue';
import Register from './pages/Register.vue';
import Note from './pages/Note.vue';

export default [
    {
        path: '/register',
        name: 'register',
        component: Register
    },
    {
        path: '/login',
        name: 'login',
        component: Login
    },
    {
        path: '/',
        name: 'home',
        component: Home
    },
    {
        path: '/:date',
        name: 'board',
        component: Board
    },
    {
        path: '/:date/plan/:id',
        name: 'plan',
        component: Plan
    },
    {
        path: '/:date/note/:id',
        name: 'note',
        component: Note
    },
]