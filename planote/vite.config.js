const { createVuePlugin } = require('vite-plugin-vue2');

// This is a built-in Node.js module. Vite and other bundlers run on Node.js
// which is why we didn't need to import it.
// path: https://nodejs.org/docs/latest-v14.x/api/path.html
// require: https://nodejs.org/en/knowledge/getting-started/what-is-require/
const path = require('path')

module.exports = {
  plugins: [createVuePlugin()],

  // What we're doing here is to add an "alias" or "nickname" for a
  // specific file so it'll be easier to write import statements,
  // specifically, we're calling restmodel.js found in lib as "sileo"
  resolve: {
    alias: {
      // __dirname is also a built-in Node.js constant.
      // https://nodejs.org/docs/latest-v14.x/api/modules.html#modules_dirname
      // path.resolve is a method to make an absolute path
      // https://nodejs.org/docs/latest-v14.x/api/path.html#path_path_resolve_paths
      'sileo': path.resolve(__dirname, './src/lib/sileo/restmodel.js')
    }
  }
};
