<img src="/planote/src/assets/images/planote_logo_46337F.png" alt="PlaNote" width="200" style="margin-left: auto;">

# MAHARLIKA - Planote

A planner productivity web application to organize users' everyday tasks.

## Scripts

```bash
  npm run dev # start dev server
  npm run build # build for production
  npm run serve # locally preview production build
```

## Team Members

* **John Roy Cabezas** - *BSCS-4*
* **Jennen Isabelle Montejo** - *BSCS-4*
* **Margaret Oquias** - *BSCS-4*
* **Marc Lloyd Quisel** - *BSCS-4*
